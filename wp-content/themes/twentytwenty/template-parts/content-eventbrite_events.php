<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php

	get_template_part( 'template-parts/entry-header-event' );

	// get_template_part( 'template-parts/featured-image' );

	?>

	<div class="post-inner">

		<div class="entry-content">

			<?php
				the_content();
			?>

		</div>
	</div>

	<?php /*
	<div class="post-inner post-inner--white">
		<div class="entry-content">
			<h6>Debbugging and prototype ideas</h6>
			<p>We might want to completely take over the presentation of events to replace the ticketing module.</p>
			<p>This would allow us control over what we show for each event in the series, as well as whether the logged in user is already attending.</p>
			<?php
			$event_meta = get_post_meta($post->ID);
			echo $event_meta['iee_event_id'][0];

			$series_events = get_event_series(144998756183);
			echo '<h4>List of event IDs in the series</h4>';
			echo '<ul>';
			foreach ($series_events as $key => $event) {
				echo '<li>' . $event->id . '</li>';
			}
			echo '</ul>';
			?>

			<?php
			$attendees = get_attendees(144999229599);
			echo '<h4>List of attendees for one in the series</h4>';
			echo '<ul>';
			foreach ($attendees as $key => $attendee) {
				echo '<li>' . $attendee->profile->first_name . ' ' . $attendee->profile->last_name . ' - ' . $attendee->profile->email . '</li>';
			}
			echo '</ul>';
			?>
		</div><!-- .entry-content -->
	</div><!-- .post-inner -->
	*/ ?>

</article><!-- .post -->
