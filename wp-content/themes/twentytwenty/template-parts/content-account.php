<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">


	<header class="entry-header has-text-align-center<?php echo esc_attr( $entry_header_classes ); ?>">
		<div class="entry-header-inner section-inner medium">

			<?php global $current_user; wp_get_current_user();
			if ( !is_user_logged_in() ) {
				echo '<h3>Welcome, visitor! Please login to continue:</h3>';
				echo '<p><a href="/wp-login.php" title="Members Area Login" rel="home">Login</a></p>';
			} else {
				echo '<h1 class="entry-title heading-size-1">Welcome ' . $current_user->user_firstname . '</h1>';
			} ?>

		</div><!-- .entry-header-inner -->
	</header><!-- .entry-header -->

	<?php

	// get_template_part( 'template-parts/featured-image' );

	?>

	<div class="post-inner">

		<div class="entry-content">

			<?php
			if ( is_user_logged_in() ) {
				the_content();

				// Events that they are signed up for
				// Performance here is rubbish, as we're making multiple calls to an API.
				// For the prototype we need to reduce the # of datetimes to reduce the impact of this.
				// Get event groups
				if ($allowed_eventbrite_groups = get_field('allowed_eventbrite_groups', 'user_'.$current_user->ID)) {
					echo '<h3>Your upcoming events</h3>';
					// echo '<div class="grid-2up">';
					$group_meta = get_post_meta($allowed_eventbrite_groups[0]);
					// Get sub events
					foreach ($allowed_eventbrite_groups as $key => $group) {
						$group_meta = get_post_meta($group);
						$series_events = get_event_series($group_meta['iee_event_id'][0]);
						// Get attendee list
						foreach ($series_events as $key => $series_event) {
// if ($key == 0) {echo '<pre>'; print_r($series_event); echo '</pre>';}
							$attendees = get_attendees($series_event->id);
							// Compare against their email
							foreach ($attendees as $key => $attendee) {
								if ($current_user->user_email == $attendee->profile->email) {
// echo '<p>' . $series_event->url . '</p>';
// echo '<h6>' . $series_event->name->text . '</h6>';
// echo '<ul>';
// echo '<li><strong>Date &amp; Time:</strong> ' . date('l jS F Y', strtotime($series_event->start->local)) . ' from ' . date('H:ia', strtotime($series_event->start->local)) . ' to ' . date('H:ia', strtotime($series_event->end->local)) . '</li>';
// echo '<li><strong>Link:</strong> <a href="https://whereby.com/dementiapeersupport" title="Join the meeting">https://whereby.com/dementiapeersupport</a></li>';
// echo '</ul>';
									?>
									<div class="card card--horizontal">
								        <div class="card__media" style="background-image: url(<?php echo get_the_post_thumbnail_url($group); ?>);"></div>
								        <div class="card__content">
								            <h3 class="card__heading"><?php echo $series_event->name->text; ?></h3>
								            <div class="card__excerpt">
								            	<?php // echo apply_filters( 'the_content', get_field( 'event_summary', $group )); ?>
								            	<?php echo '<p><strong>Date &amp; Time:</strong> ' . date('l jS F Y', strtotime($series_event->start->local)) . ' from ' . date('H:ia', strtotime($series_event->start->local)) . ' to ' . date('H:ia', strtotime($series_event->end->local)) . '</p>'; ?>
								            	<?php if (get_field('meeting_information',$group)): ?>
								            		<?php echo apply_filters( 'the_content', get_field('meeting_information',$group) ) ?>
								            	<?php endif ?>
								            	<?php if (get_field('meeting_link',$group)): ?>
								            		<p><a href="<?php echo get_field('meeting_link',$group); ?>" class="button" title="Join the meeting">Join the meeting</a></p>
								            	<?php endif ?>
								            </div>
								        </div>
								    </div>
									<?php
								}
							}
						}
					}
					// echo '</div>';
				}

				show_all_events('Other events');

			}
			?>

		</div><!-- .entry-content -->

	</div><!-- .post-inner -->

</article><!-- .post -->
